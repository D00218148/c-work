#pragma once
#include <string>
#include <iostream>

using namespace std;

class ClockType
{
	int hours;
	int minutes;
	int seconds;
public:
	ClockType();
	void IncrementSecondsBy(int n);
	void IncrementMinutesBy(int n);
	void IncrementHoursBy(int n);
	string getCurrentTime();
	void displayCurrentTime();
	void resetClock();
	void setTime(int hh, int mm, int ss);
	int getElapsedTimeSeconds();
	int getRemainingTimeSeconds();
	void getDifference(ClockType& other);
	//overloading operators
	void operator+(int x);
	ClockType operator+(ClockType& other);
	friend ostream& operator<<(ostream& out, ClockType& ct);
	~ClockType();
};
// Classes.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include "StudentType.h"
#include "DayType.h"
#include "ClockType.h"

using namespace std;
void question1();
void question2();
void question3();
void display(StudentType&);

int main()
{
    question3();
    cout << "main finished" << endl;
}

void question3()
{
    ClockType ct;
    ct.setTime(23, 59, 59);
    ct.displayCurrentTime();
    //ct.IncrementSecondsBy(2);
    //ct.displayCurrentTime();
    cout << "Current time in seconds: " << ct.getElapsedTimeSeconds() << endl;
    cout << "Remaining time in day in seconds: " << ct.getRemainingTimeSeconds() << endl;
    ClockType ct2;
    ct2.setTime(16, 42, 37);;
    ct.getDifference(ct2);

    ct + 60;
    cout << "using overloaded + operator: ";
    ct.displayCurrentTime();
    cout << endl;

    ClockType ct3, ct4;
    ct3.setTime(12, 10, 10);
    ct4.setTime(1, 1, 1);
    ClockType ct5 = ct3 + ct4;
    //ct5.displayCurrentTime();
    cout << ct5 << endl;
}

void question2()
{
    DayType dt;
    cout << dt.getXDaysInFuture(8) << endl;

    cout << "Current day: " << dt.getCurrentDay() << endl;
    cout << "Next day: " << dt.getNextDay() << endl;
    cout << "Previous day: " << dt.getPreviousDay() << endl;

    dt.setCurrentDay(5);
    cout << "New current day: " << dt.getCurrentDay() << endl;
    dt.printCurrentDay();
}

void question1()
{
    StudentType s; //will use default constructor
    display(s);
    StudentType s2(2, "John Doe", 18, 1.8); //will use custom constructor
    display(s2);

    //testing setters
    s.setId(10);
    s.setAge(20);
    s.setHeight(2.0);
    s.setName("Jeremy Crow");
    display(s);
}

void display(StudentType& s)
{
    cout << string(20, '*') << endl;
    cout << "ID: " << s.getId() << endl;
    cout << "Name: " << s.getName() << endl;
    cout << "Age: " << s.getAge() << endl;
    cout << "Height:" << s.getHeight() << endl;
    cout << string(20, '*') << endl;
}
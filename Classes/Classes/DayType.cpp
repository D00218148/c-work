#include "DayType.h"
#include <iostream>

using namespace std;

DayType::DayType()
{
	currentDay = 0;
	days = new string[7]{ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
}

void DayType::setCurrentDay(int cd)
{
	if (cd >= 0)
	{
		currentDay = cd % 7;
	}
}

void DayType::printCurrentDay()
{
	cout << days[currentDay] << endl;
}

string DayType::getCurrentDay()
{
	return days[currentDay];
}

string DayType::getNextDay()
{
	return days[(currentDay + 1)%7];
}

string DayType::getPreviousDay()
{
	return days[(currentDay + 6) % 7];
}

string DayType::getXDaysInFuture(int x)
{
	return days[(currentDay + x) % 7];
}

DayType::~DayType()
{
	delete[] days;
}
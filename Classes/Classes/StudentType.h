#pragma once
#include <string>

using namespace std;

class StudentType
{
	int id, age;
	string name;
	double height;
	
public:
	StudentType(); //default constructor
	StudentType(int id, string name, int age, double height); //parameter constructor
	
	//getters
	int getId();
	int getAge();
	string getName();
	double getHeight();

	//setters
	void setId(int id);
	void setAge(int age);
	void setName(string name);
	void setHeight(double height);

	~StudentType(); //destructor(required to end class)
}; //; defines end of class

//constructors
StudentType::StudentType() //:: used to get a method belonging to a class
{
	this->id = 1;
	this->name = "Default";
	this->age = 0;
	this->height = 0.0;
}

StudentType::StudentType(int id, string name, int age, double height) //parameter constructor
{
	this->id = id; //this-> same as this.
	this->name = name;
	this->age = age;
	this->height = height;
}

//getters
int StudentType::getId()
{
	return this->id;
}
int StudentType::getAge()
{
	return this->age;
}
string StudentType::getName()
{
	return this->name;
}
double StudentType::getHeight()
{
	return this->height;
}

//setters
void StudentType::setId(int id)
{
	if (id > 0)
	{
		this->id = id;
	}
}
void StudentType::setAge(int age)
{
	if (age > 0 && age < 110)
	{
		this->age = age;
	}
}
void StudentType::setName(string name)
{
	if (name.length() > 1)
	{
		this->name = name;
	}
}
void StudentType::setHeight(double height)
{
	if (height > 1)
	{
		this->height = height;
	}
}

//destructor
StudentType::~StudentType()
{
	cout << "destructor called" << endl; // frees up memory used by am object of student type
}
#pragma once
#include <string>

using namespace std;

class DayType
{
	int currentDay;
	string *days;
public:
	DayType();
	void setCurrentDay(int day);
	void printCurrentDay();
	string getCurrentDay();
	string getNextDay();
	string getPreviousDay();
	string getXDaysInFuture(int x);
	~DayType();
};
#include "ClockType.h"
#include <iostream>

using namespace std;

ClockType::ClockType()
{
	hours = 00;
	minutes = 00;
	seconds = 00;
}

void ClockType::IncrementSecondsBy(int n)
{
	if (n > 0)
	{
		seconds += n;
		while (seconds >= 60)
		{
			IncrementMinutesBy(1);
			seconds -= 60;
		}
	}
}

void ClockType::IncrementMinutesBy(int n)
{
	if (n > 0)
	{
		minutes += n;
		while (minutes >= 60)
		{
			IncrementHoursBy(1);
			minutes -= 60;
		}
	}
}

void ClockType::IncrementHoursBy(int n)
{
	if (n > 0)
	{
		hours += n;
		while (hours >= 24)
		{
			hours -= 24;
		}
	}
}

string ClockType::getCurrentTime()
{
	return to_string(hours) + ":" + to_string(minutes) + ":" + to_string(seconds);
}

void ClockType::displayCurrentTime()
{
	cout << getCurrentTime() << endl;
}

void ClockType::resetClock()
{
	hours = 0;
	minutes = 0;
	seconds = 0;
}

void ClockType::setTime(int hh, int mm, int ss)
{
	if (hours < 24 && minutes < 60 && seconds < 60)
	{
		hours = hh;
		minutes = mm;
		seconds = ss;
	}
}

int ClockType::getElapsedTimeSeconds()
{
	int totalTimeInSeconds = (hours * 60 * 60) + (minutes * 60) + seconds;
	return totalTimeInSeconds;
}

int ClockType::getRemainingTimeSeconds()
{
	return 86400 - this->getElapsedTimeSeconds();
}

void ClockType::getDifference(ClockType& other)
{
	int temp = abs(other.getElapsedTimeSeconds() - this->getElapsedTimeSeconds());
	int hh = temp / 3600;
	int mm = (temp % 3600) / 60;
	int ss = temp % 60;
	cout << "Difference: " << hh << ":" << mm << ":" << ss << endl;
}

//overloading operators in this case the +
void ClockType::operator+(int x)
{
	this->IncrementSecondsBy(x);
}

ClockType ClockType::operator+(ClockType& other)
{
	ClockType newCT(*this);
	newCT.IncrementSecondsBy(other.seconds);
	newCT.IncrementMinutesBy(other.minutes);
	newCT.IncrementHoursBy(other.hours);
	return newCT;
}

//overloading output stream operator
ostream& operator<<(ostream& out, ClockType& ct)
{
	out << ct.getCurrentTime();
	return out;
}

ClockType::~ClockType()
{

}
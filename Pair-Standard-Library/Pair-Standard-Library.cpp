// Pair-Standard-Library.cpp

// Demonstrates use of a pair<T1,T2> struct from <utility> library.
// Can store a pair of any type of object.

#include <utility>      // pair
#include <iostream>     // cout
using namespace std;

int main()
{
    cout << "Demo class pair<>\n";

    // Let's say we want to store an (x,y) coordinate.
    // This is a natural pair of elements.
    // We could create our own struct called Coordinate with 
    // x and y member fields.
    // However, C++ provides a 'pair' struct for us that 
    // achieves the same purpose.  
    // Member fields are public so can be accessed directly.
    // It is a template class, so we specify the types of the 
    // first and second item in the pair.

    // Initialize pair objects

    pair<int, int> c1{ 2,3 };   // initialization list
    pair<int, int> c2( 4,5 );   // constructor
    
    pair<int, int> c3 = c1;     // assignment

    cout << "Coordinates:" << endl;
    cout << "c1: (" << c1.first << "," << c1.second << ")\n";
    cout << "c2: (" << c2.first << "," << c2.second << ")\n";
    cout << "c3: (" << c3.first << "," << c3.second << ")\n";

    c2.first = 8;   // reset individual fields
    c2.second = 9;

    // operator== is implemented in pair<>
    if (c1 == c3)
        cout << "(c1 == c3) is true, they are equal.\n";
    else
        cout << "They are NOT the same.\n";

    // pair with name and age

    pair<string, int> p1{ "Tom",21 };  // name and age pair
    pair<string, int> p2{ "Tom",21 };
    if (p1 == p2)
        cout << "p1==p2 is true";
}

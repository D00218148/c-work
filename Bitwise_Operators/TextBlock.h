#pragma once
#include <iostream>
#include <string>
using namespace std;

class TextBlock
{
	const int boldMask = 1;				// 0x00000001
	const int italicMask = 2;			// 0x00000010
	const int underlinedMask = 4;		// 0x00000100
	const int subscriptMask = 8;		// 0x00001000
	const int highlightMask = 16;		// 0x00010000
	const int strikethroughMask = 32;	// 0x00100000
	string text;
	int flags;
public:
	TextBlock(string s);
	bool isBold();
	bool isItalic();
	bool isUnderlined();
	bool isHighlighted();
	bool isSubscript();
	bool isStrikethrough();

	void setBold(bool val);
	void setItalic(bool val);
	void setUnderlined(bool val);
	void setHighlighted(bool val);
	void setSubscript(bool val);
	void setStrikethrough(bool val);

	void print();
	~TextBlock();	// destructor
};


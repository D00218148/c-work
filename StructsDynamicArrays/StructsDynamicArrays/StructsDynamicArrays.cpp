// StructsDynamicArrays.cpp : This file contains the 'main' function. Program execution begins and ends there.
//


#include <iostream>
using namespace std;

struct movies_t 
{
	string title;
	int year;
};

struct fruit_t
{
	string name;
	string colour;
};

struct array_list
{
	int* arr;
	int capacity;
	int count;
};

void display(const movies_t &movie);
void displayPointer(const movies_t* pMovie);
void displayAll(const movies_t*, int);
void displayFruit(const fruit_t& fruit);
void add(array_list& list, int x);
void create(array_list& list, int size);
void pack(array_list& list);
void print(array_list& list);
void remove(array_list& list, int pos);

int main()
{
	movies_t movie;
	movie.title = "Simpsons Movie";
	movie.year = 2012;

	display(movie);
	displayPointer(&movie);

	movies_t movies[4];
	movies[0] = movie;
	movies[1].title = "Spongebob Movie";
	movies[1].year = 2060;
	movies[2].title = "Friday the 13th";
	movies[2].year = 11;
	movies[3].title = "Chucky";
	movies[3].year = 1990;

	displayAll(movies, 4);

	fruit_t fruit;
	fruit.name = "Banana";
	fruit.colour = "Red";

	displayFruit(fruit);

	array_list list;
	create(list, 5);
}

void remove(array_list& list, int pos)
{
	for (int i = 0; i < list.count - 1; i++)
	{
		list.arr[i] = list.arr[i + 1];
	}
	list.count--;
}

void print(array_list& list)
{
	for (int i = 0; i < list.count; i++)
	{
		cout << list.arr[i] << " ";
	}
	cout << endl;
}

void pack(array_list& list)
{
	if (list.count < list.capacity)
	{
		int* temp = new int[list.count];
		for (int i = 0; i < list.count; i++)
		{
			temp[i] = list.arr[i];
		}
		delete[] list.arr;
		list.arr = temp;
	}
}

void create(array_list& list, int size)
{
	list.arr = new int[size];
	list.count = 0;
	list.capacity = size;
}

void add(array_list& list, int x)
{
	if (list.count >= list.capacity)
	{
		int* temp = new int[list.capacity * 2];
		for (int i = 0; i < list.count; i++)
		{
			temp[i] = list.arr[i];
		}
		delete[] list.arr;
		list.arr = temp;
		list.capacity *= 2;
	}
	list.arr[list.count] = x;
	list.count++;
}

void displayFruit(const fruit_t& fruit)
{
	cout << fruit.name << "(" << fruit.colour << ")" << endl;
}

void display(const movies_t &movie)
{
	cout << movie.title << "(" << movie.year << ")" << endl;
}

void displayPointer(const movies_t* movie)
{
	cout << (*movie).title << "(" << movie->year << ")" << endl;
}

void displayAll(const movies_t* pMovie, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << pMovie->title << "(" << pMovie->year << ")" << endl;
		pMovie++;
	}
}
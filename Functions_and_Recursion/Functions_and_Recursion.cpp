// Functions_and_Recursion.cpp 
/*
Demonstrates:
Function calls with parameters
Stack based variables (i.e. automatic variables) created and removed from the stack
Call-stack (Stack Frame) (Activation Records) [ you must draw diagrams of these ]
Call-by-Value and Call-by-Reference
Recursion

*/
#include <iostream>
using namespace std;

// function prototypes
void function1();
int square(int); // prototype for function square
int sum(int, int);

int recursiveSum(int n);
int factorial(int a);

void demoPassByValue(int, double);
void demoPassByReference(int& x, double& z);	// reference parameters

int main()
{
	std::cout << "Function calls and Stack Variables\n";
	function1();	// call a function that returns void (i.e. no return value)

	int a = 10; // value to square (local automatic variable in main)
	cout << a << " squared: " << square(a) << endl; // display a squared

	int result;
	int x = 3;
	int y = 4;
	result = sum(x, y);
	cout << "sum(3,4) = " << result << endl;

	// Recursion 
	result = recursiveSum(3);  // sum up values from 3 down to 1
	cout << "recursiveSum( 3 ) = " << result << endl;

	int number = 4;
	cout << number << " factorial (4!) = " << factorial(number) << endl;

	double d = 3.14;

	demoPassByValue(x, d);
	cout << "after calling demoCallByValue() x=" << x << ", d=" << d << endl;

	demoPassByReference(x, d);
	cout << "after calling demoCallByValue() x=" << x << ", d=" << d << endl;
	cout << "x and y have been changed by the function, using references." << endl;

	return 0;  // indicate successful termination of main()
}

void function1()
{
	int x = 4;
	cout << "In function1(), x = " << x << endl;
	// 2DO: draw the call-stack at this point
}

// returns the square of an integer
int square(int x)	// x is a parameter  (a local variable in this function)
{
	return x * x;  // "2DO: draw the call stack just before this returns
}

int sum(int x, int y)
{
	int sum = x + y;
	return sum;		// 2DO: draw the stack frame with variables at this point
}

/*
	To understand recursiveSum() you must think of summing the numbers in this way:
	To sum all numbers from 3 down to 1
	   result = recursiveSum( 3 )
			          3           + recursiveSum( 2 )
	which is:         3           +       2          + recursiveSum( 1 )
	which is:         3           +       2          +        1
*/

int recursiveSum(int n)
{
	if (n == 1)			// terminating condition
		return 1;		// 

	return n + recursiveSum(n - 1);  // recursive call
}

int factorial(int a)
{
	if (a > 1)
		return (a * factorial(a - 1));

	return 1;	// 2DO: Draw a call-stack at this point of execution
}

void demoPassByValue(int x, double z)
{
	cout << "x = " << x << endl;
	x = 777;	// Changes the local x, but does NOT change the value of x in main()
				// 2DO: Draw the call stack, and you will see why this is so.

	z = 8.11;    // has no effect on value of y in main()
}

// Pass by Reference  - (using Reference Parameters)
void demoPassByReference(int& x, double& z)
{
	// Parameters - x is a reference to an int, and it is bound to x in main() when called
	//              z is a reference to a double, and is bound to variable d from main() when called

	x = 2000;	// This WILL change the value of x in main, because this x is 
				// a reference parameter that refers to the variable x in main()
				// 2DO: Draw the Call-Stack, and you will see why this is so.

	z = 8.11;    // This will change the value of y in main(), because z is a reference to y in main()
}



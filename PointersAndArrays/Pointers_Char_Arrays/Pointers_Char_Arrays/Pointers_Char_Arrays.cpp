// Pointers_Char_Arrays.cpp : 

/*
Demonstrates a C-style string, which is an array of chars.
Using Pointers to access an array of 'char'

NB: Strings in C are stored in an array of char, terminated with a null value i.e '\0'
Note that there is a string class in C++ that makes dealing with strings easy,
but many systems still use the older C-style array of chars.
*/

#include <iostream>
using namespace std;
void increment(double*);
double divide(const int*, const int*);
double sumArr(double*, int);
double sumPointer(double*, int);
void doubleElements(int*, int);
int strcmp(const char*, const char*);

int main()
{
	char name[20] = "James";
	// The char array name[] is filled with the characters 'J', 'a', ....
	// and terminated by a null character '\0'
	// The '\0' character is added to the end of the string (i.e, as the sixth character)

	char town[] = { 'A', 'r', 'd', 'e', 'e', '\0' };
	// in this case we explicitly filled each character. We must put in the '\0'
	// in this case. The size of the array will be 6 in this case.

	cout << name << endl;
	cout << town << endl;

	// Print out the characters one-by-one
	int i = 0;
	while (name[i] != '\0')		// while we have not reached the '\0'
	{
		cout << name[i];  // output the character
		i++;
	}
	cout << endl;

	// Print out the characters using a pointer
	char* p;	// p is a pointer to a char
	p = name;	// name of an array is the address of first array element
				// so, p now points at the first character ( i.e. 'J' )

	while (*p != '\0')	// Iterate over the characters using a pointer
	{
		cout << *p;
		p++;
	}
	cout << endl << endl;

	// We can write 'difficult to read' code in C, which is a novelty, but
	// doesn't do much for program clarity.
	// What do you think the while() loop below does?

	char buffer[40];			// temporary storage array
	char* pBuffer = buffer;		// pointer to buffer
	p = name;

	//while (*pBuffer++ = *p++);  // yes, it's all in one line !
	while (*p != '\0')
	{
		*pBuffer = *p;
		p++;
		pBuffer++;
	}
	*pBuffer = '\0';

	cout << "Buffer contains: " << buffer << endl;

	// The above is BAD practice because it is difficult to understand.

	//*** Exercise - add the code for each question at the end of the question.
	//    Compile run and test as you go. Study the output carefully.

	// Q.1.
	// Declare an array called nameAndTown, i.e. char nameAndTown[50];
	// Now fill the nameAndTown array with the name from name[], 
	// followed by a comma, and then by a space, and then by the
	// townname from the town array.  
	// Remember to terminate the string with a '\0'.  
	// Print out your result.

	// a. Use array notation.
	// b. Use pointer notation.

	char nameAndTown[50];
	char* pNameAndTown = nameAndTown;

	p = name;
	while (*p != '\0')
	{
		
		*pNameAndTown = *p;
		pNameAndTown++;
		p++;
	}

	*pNameAndTown = ',';
	pNameAndTown++;
	*pNameAndTown = ' ';
	pNameAndTown++;

	p = town;
	while (*p != '\0')
	{
		*pNameAndTown = *p;
		pNameAndTown++;
		p++;
	}
	*pNameAndTown = '\0';

	cout << nameAndTown << endl;

	char nat2[50];
	int natPos = 0;
	int e = 0;
	while (name[e] != '\0')
	{
		nat2[natPos] = name[e];
		e++;
		natPos++;
	}
	nat2[natPos] = ' ';
	natPos++;
	e = 0;
	while (town[e] != '\0')
	{
		nat2[natPos] = name[e];
		e++;
		natPos++;
	}
	nat2[natPos] = '\0';

	cout << nat2 << endl << endl;

	double d = 3.14;
	double* pd = &d;
	cout << "d address: " << &d << endl;
	cout << "d Before increment: " << d << endl;
	increment(&d);
	cout << "d After increment: " << d << endl;

	int val1 = 10;
	int val2 = 5;
	double result = divide(&val1, &val2);
	cout << "Result: " << result << endl;

	double arr[5] = { 1.1, 2.2, 3.3, 4.4, 5.5 };

	double totalArr = sumArr(arr, 5);
	double totalPointer = sumPointer(arr, 5);
	//cout << sizeof(arr)/sizeof(arr[0]) gives size of array

	cout << "Sum(Array notation): " << totalArr << endl;
	cout << "Sum(Pointer notation): " << totalPointer << endl;

	int nums[10] = { 1,2,3,4,5,6,7,8,9,10 };
	doubleElements(nums, 10);
	for (int i = 0; i < 10; i++)
	{
		cout << nums[i] << ",";
	}
	cout << endl;

	char str1[] = "Are these the saoe?";
	char str2[] = "Are these the sane?";
	int cmpResult = strcmp(str1, str2);
	cout << cmpResult << endl;

	return 0;
}

void increment(double* pd)
{
	cout << "pointer address: " << pd << endl;
	cout << "value in pointer: " << *pd << endl;
	*pd += 0.01;
}

double divide(const int* p1, const int* p2)
{
	return (double)(*p1 / *p2);
}

double sumArr(double* arr, int size)
{
	double sum = 0;
	for (int i = 0; i < size; i++)
	{
		sum += arr[i];
	}
	return sum;
}

double sumPointer(double* pArr, int size)
{
	double sum = 0;
	double* pStart = pArr;
	for (int i = 0; i < size; i++)
	{
		sum += *pArr;
		pArr++;
	}
	pArr = pStart;
	return sum;
}

void doubleElements(int* pArr, int size)
{
	for (int i = 0; i < size; i++)
	{
		*pArr = *pArr * 2;
		pArr++;
	}
}

int strcmp(const char* s1, const char* s2)
{
	while (*s1 != '\0' && *s2 != '\0' && *s1 == *s2)
	{
		s1++;
		s2++;
	}
	return *s1 > * s2 ? -1 : *s1 == *s2 ? 0 : 1;
}
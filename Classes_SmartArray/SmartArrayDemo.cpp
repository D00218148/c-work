#include "SmartArray.h"
#include <iostream>
// Dynamic Array using a "SmartArray" Class

void demoDynamicallyAllocatedArray();
void demoSmartArray();
void demoAssignmentOperator();
void demoCopyConstructor();

using namespace std;

int main()
{
	
	//std::cout << "**************************** demoDynamicallyAllocatedArray ****************************" << std::endl << std::endl;
	//demoDynamicallyAllocatedArray();

	std::cout << "**************************** demoSmartArray ****************************" << std::endl << std::endl;
	demoSmartArray();

	//std::cout << "**************************** demoAssignmentOperator ****************************" << std::endl << std::endl;
	//demoAssignmentOperator();

	//std::cout << "**************************** demoCopyConstructor ****************************" << std::endl << std::endl;
	//demoCopyConstructor();
	//

}

// demonstrates that if we use new or malloc to allocate a block of memory
// to store an array, then we need to always remember to manually call delete 
// (or free() if using malloc() - see below)

void demoDynamicallyAllocatedArray()
{
	cout << "demoDynamicallyAllocatedArray()" << endl;

	int size = 5;
	int* pArr = new int[size];	// dynamically allocate array of 5 int
	pArr[0] = 5;
	pArr[1] = 10;
	pArr[2] = 15;
	pArr[3] = 20;
	pArr[4] = 25;

	for (int i = 0; i < size; i++) {
		if (i > 0) std::cout << ",";
		std::cout << pArr[i];
	}
	cout << endl;

	cout << "Freeing our dynamically allocated array using delete[]..." << endl;

	// we don't like using this approach because we may forget to delete 
	// (or free()) and get a MEMORY LEAK!!!
	delete[] pArr;
	pArr = nullptr;	// not essential, but good practice to prevent unintended use

	/*
	//Remember we can also use malloc and free() (old-style C )
	int size = 5;
	int* pArrb = (int*)malloc(sizeof(int) * size);
	free(pArrb);
	*/
}

void demoSmartArray()
{
	SmartArray sArr(4);	// SmartArray implements a dynamic array (one that can grow!)
	cout << "Populating array with elements...\n";
	sArr.add(1111);
	sArr.add(2222);
	sArr.add(3333);
	sArr.add(4444);
	sArr.add(5555);	// array should grow internally, triggered by this insertion
	
	cout << "SmartArray sArr: \n";
	sArr.print();

	int x = sArr.get(1);
	cout << "sArr.get(1) returns element: " << x << endl;
}

// Demonstrates how the overloaded assignment operator= creates 
// a distinct copy of the target object including a distinct 
// copy of any heap-based memory .
// This is called a "Deep Copy" of the object.
void demoAssignmentOperator()
{
	SmartArray s1(4);	// capacity 4 elements
	s1.add(1111);
	s1.add(2222);
	s1.add(3333);

	SmartArray s2;		// calls explicit default constructor
	s2 = s1;			// overloaded assignment operator is invoked
	
	cout << "s1: ";
	s1.print();
	cout << "s2: ";
	s2.print();

	SmartArray s6;  // default no-arg constructor is called.
	s6.print();
}

// Demonstrates how the copy constructor creates a distinct 
// (i.e. deep copy) of the target object
void demoCopyConstructor()
{
	SmartArray s1(4);	// normal constructor invoked
	s1.add(1111);
	s1.add(2222);
	s1.add(3333);

	SmartArray s2(s1); // Copy Constructor is invoked
	s2.print();

	SmartArray s3 = s1;  // Copy Constructor invoked (NOT the assignment operator=)
	s3.print();
}

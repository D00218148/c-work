#pragma once
 /*  SmartArray.cpp 
 This SmartArray implementation shows how a dynamic array could be 
 implemented.  It is similar to how the C++ "vector" and the 
 Java "ArrayList" containers are implemented.

 This can only be implemented by using a pointer member variable that points to 
 a dynamically allocated array block, that can be reallocated when required.

 When useing a pointer mamber variable we MUST implement:
		 1. Copy Constructor
		 2. Overloaded Assignment operator=
		 3. Destructor
to deal with the dynamically allocated memory.

 */

#include <iostream>
#include <string>
#include <time.h>

// best not to put "using namespace std" in a header file, as it 
// exposes the namespce to all files that #include this header

class SmartArray
{
private:
	int* pArray;	// pointer to array of int values (the Heap-based array)
	int size;		// current number of values in the array
	int capacity;	// total number of slots in the array

public:
	// Constructor
	SmartArray(int capacity)
	{
		if (capacity <= 0)
			throw std::out_of_range("Size cannot be <= 0");

		pArray = new int[capacity];
		this->capacity = capacity;
		size = 0;	// initially no elements added to the array
	}

	// explicit no-argument default Constructor
	SmartArray()
	{
		capacity = 4;
		pArray = new int[capacity];	// default size is 4 
		size = 0;				// no elements in the array yet
	}

	// Copy Constructor - required for 'deep copy' 
	//
	SmartArray(SmartArray& other)
	{
		pArray = new int[other.capacity];

		for (int i = 0; i < other.size; i++)
			pArray[i] = other.pArray[i];

		this->capacity = other.capacity;
		this->size = other.size;
	}

	//overloaded assignment operator=   (  deep copy )
	//
	SmartArray& operator=(const SmartArray& other)
	{
		// self-assignment guard (if assigning object to itself. e.g. s1=s1;)
		if (this == &other)		// don't do anything, and 
			return *this;		// return reference to this object

		// The dynamic array belonging to this object is no longer
		// needed, so, free up the memory belonging to it (if any)
		if (pArray != nullptr) delete[] pArray;

		// dynamically allocate a new array block for the THIS object
		pArray = new int[other.capacity];

		// copy data from other object's array to this object's array
		for (int i = 0; i < size; i++)
			pArray[i] = other.pArray[i];

		// copy member data from the other object into
		// the member variables (fields) of this object
		size = other.size;
		capacity = other.capacity;
		// members of the same class can access each others fields directly

		return *this; // de-reference the pointer returning a reference to this object
	}

	// Destructor - called when SmartArray object goes out of scope
	~SmartArray()
	{
		delete[] pArray;	// free up the dynamic memory.
							// forgetting this causes memory leak!
	}

	int getCapacity() const
	{
		return capacity;
	}

	int getSize() const
	{
		return size;
	}

	int get(int i)
	{
		if( i<0 || i>=size) 
			throw std::out_of_range("Index out of range.");
		else
			return pArray[i];
	}

	// add an element to the smart array and grow the array if necessary
	void add(int value)
	{
		if (size >= capacity)  // at full capacity, so need to grow
		{
			int* pTemp = new int[capacity * 2];  // allocate a new array, double the size

			for (int i = 0; i < size; i++)
				pTemp[i] = pArray[i];   // copy data to new array

			capacity = capacity * 2;  // set the new capacity value (double the previous)

			delete[] pArray;   // free old memory (forgetting this will cause a memory leak)

			pArray = pTemp;   // assign new array block to the array pointer

			pArray[size] = value; // add the new element 
			size++;

			std::cout << "*** Expanded array to new capacity: " << capacity << std::endl;
		}
		else  // else, there is free space, so simply insert the new value
		{
			pArray[size] = value;	// size is the number of elements and the index of the next free space
			size++;
		}
	}
	
	int getSize()
	{
		return size;
	}

	void print()
	{
		for (int i = 0; i < size; i++) {
			if (i > 0) std::cout << ",";
			std::cout << pArray[i];
		}
		std::cout << std::endl;
	}
};
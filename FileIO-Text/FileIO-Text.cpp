// FileIO-Text.cpp
// Basic Text File IO - write and read
// Demonstrates:
//    Writing to a text file using an output file stream
//    Reading from a text file line-by-line, input file stream
//    Parsing a comma delimited line of text using a stringstream
//    Using <string> functions:  getline(), stoi() and stod()

#include <iostream>
#include <fstream>      // file stream
#include <string>       // getline(), stoi(), stod()
#include <sstream>      // string stream
using namespace std;

void DemoOutputFileStream() 
{
    cout << "Creating and writing to file: out.txt" << endl;

    ofstream outStream("out.txt");  // write mode (overwrites existing data)

    if ( outStream.good() )
    {
        outStream << "This is a line of text.\n";
        outStream << "This is another line of text.\n";
        int age = 19;
        double height = 1.85;
        outStream << "John" << "," << age << "," << height << endl;
        
        outStream.close(); //  close file 
        cout << "File written.\n" << endl;
    }
    else
        cout << "Unable to open file";
}

/* 
* Parse a comma-delimited string and output each field.
* We know the string format is: "name,age,height"
*/
void parseLine(const string& str) {

    stringstream strStream( str ); //create string stream from the string
    string name;
    getline(strStream, name, ',');

    int age = 0;
    double height = 0.0;

    try
    {
        string str;
        getline(strStream, str, ',');

        age = stoi(str); // string to int (throws exceptions)
        
        getline(strStream, str, ',');

        height = stod(str); // string to double (throws exceptions)
    }
    catch (std::invalid_argument const& e)
    {
        cout << "Bad input: std::invalid_argument thrown" << '\n';
    }
    catch (std::out_of_range const& e)
    {
        cout << "Integer overflow: std::out_of_range thrown" << '\n';
    }

    cout << "Name: " << name << " age: " << age << " height: " << height << endl;
}

/*
* Open text file as an input file stream (ifstream)
* Read text line-by-line from the file 
* Parse each line of text.
*/
void DemoInputFileStream() {
    cout << "Reding from comma-delimited text file." << endl;

    string line;
    ifstream inStream("students.txt"); // open file as input file stream

    if ( inStream.good() )  // if file opened successfully, and not empty
    {
        while ( getline( inStream, line) )   // read a line until false returned , getline() from <string> library
        {
            parseLine( line );
        }
        inStream.close();
    }
    else 
        cout << "Unable to open file, or file is empty.";
}

int main() {
    DemoOutputFileStream();
    DemoInputFileStream(); // read and parse lines 
    return 0;
}
// CPP_CA2_ZalisauskasAntanas.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>

using namespace std;

struct message_t
{
    int sourceAddress;
    int destinationAddress;
    short sourcePort;
    short destinationPort;
    string messagePayload;
};

int split(int ipAddress, int r1, int r2, int r3, int r4);
void displayMessage(const message_t* pMessage);
int getHeader(const message_t* pMessage);
int factorial(int n);

int main()
{
    //Q1
    message_t message;
    message.sourceAddress = 0x000044ff;
    message.destinationAddress = 0xff330000;
    message.sourcePort = 0x00ff;
    message.destinationPort = 0xab00;
    message.messagePayload = "Hello";

    cout << "Source Address: " << message.sourceAddress << endl;
    cout << "Destination Address: " << message.destinationAddress << endl;
    cout << "Source Port: " << message.sourcePort << endl;
    cout << "Destination Port: " << message.destinationPort << endl;
    cout << "Message Payload: " << message.messagePayload << endl;

    //Q2
    cout << endl;
    int ipAddress = 0xff0fff00;
    int r1 = 0x00;
    int r2 = 0x00;
    int r3 = 0x00;
    int r4 = 0x00;

    cout << "Split: " << hex << split(ipAddress, r1, r2, r3, r4) << dec << endl;

    //Q3
    cout << endl;
    displayMessage(&message);

    //Q4
    cout << endl;
    //int* arr = new int[4];
    getHeader(&message);

    //Q5
    cout << endl;
    cout << "Factorial of 4: " << factorial(4) << endl;
}

//Q5
int factorial(int n)
{
    if (n > 1)
        return (n * factorial(n - 1));
    
    return 1;
}

//Q4
int getHeader(const message_t* pMessage)
{
    int* temp = new int[4];
    temp[0] = pMessage->sourceAddress;
    temp[1] = pMessage->destinationAddress;
    temp[2] = temp[2] | (pMessage->sourcePort << 16);
    temp[2] = temp[2] | (pMessage->destinationPort);
    //temp[3] = pMessage->messagePayload.length;
    
    cout << hex << temp[0] << endl;
    cout << temp[1] << endl;
    cout << temp[2] << dec << endl;
    //cout << temp[3] << endl;

    return *temp;
}

//Q3
void displayMessage(const message_t* pMessage)
{
    cout << "Source: " << pMessage->sourceAddress << endl;
    cout << "Destination: " << pMessage->destinationAddress << endl;
    cout << "Message: " << pMessage->messagePayload << endl;
}

//Q2
int split(int ipAddress, int r1, int r2, int r3, int r4)
{
    int result;
    result = r1 | (ipAddress << 24);
    result = r2 | (ipAddress << 16);
    result = r3 | (ipAddress << 8);
    result = r4 | ipAddress;
    return result;
}
#pragma once
#include "Calculateable.h"
#include "Drawable.h"
#include <iostream>

using namespace std;

class Shape2: public Drawable, public Calculateable
{
	string name;
public:
	Shape2(string name = "shape") { this->name = name; };
	virtual double area() = 0;
	virtual double perimiter() = 0;
	virtual void draw() = 0;
	~Shape2();
};


#include "Animal.h"

Animal::Animal(string name)
{
	this->name = name;
}
void Animal::print()
{
	cout << name << endl;
}
void Animal::speak()
{
	cout << "not sure how to speak" << endl;
}
string Animal::getName()
{
	return this->name;
}
ostream& operator<<(ostream& out, Animal& other)
{
	other.print();
	return out;
}
Animal::~Animal()
{

}
#include "Student.h"

Student::Student(string name, string ppsNumber, int studentid) : Person(name, ppsNumber)
{
	this->studentid = studentid;
}

void Student::print()
{
	cout << this->getName() << " " << this->getPPSNumber << " " << studentid << endl;
}

Student::~Student()
{

}
#pragma once
#include <iostream>

using namespace std;

class Animal
{
	string name;
public:
	Animal(string name = "default");
	void print();
	virtual void speak();
	string getName();
	friend ostream& operator<<(ostream& out, Animal& other);
	~Animal();
};


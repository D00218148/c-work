#pragma once
#include "Animal.h"

using namespace std;

class Dog : public Animal
{
	string breed;
public:
	Dog(string name, string breed);
	void print();
	void speak();
	~Dog();
};


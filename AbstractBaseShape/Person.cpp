#include "Person.h"

Person::Person(string name, string ppsNumber)
{
	this->name = name;
	this->ppsNumber = ppsNumber;
}

void Person::print()
{
	cout << name << " " << ppsNumber << endl;
}

string Person::getName()
{
	return this->name;
}

string Person::getPPSNumber()
{
	return this->ppsNumber;
}

Person::~Person()
{
	
}
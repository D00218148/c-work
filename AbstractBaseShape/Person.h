#pragma once
#include <iostream>

using namespace std;

class Person
{
	string name;
	string ppsNumber;
public:
	Person(string name, string ppsNumber);
	virtual void print();
	string getName();
	string getPPSNumber();
	~Person();
};
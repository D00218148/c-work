#include "Dog.h"

using namespace std;

Dog::Dog(string name, string breed) : Animal(name)
{
	this->breed = breed;
}
void Dog::print()
{
	cout << this->getName() << " the dog is a " << breed << endl;
}
void Dog::speak()
{
	cout << "Woof" << endl;
}
Dog::~Dog()
{

}
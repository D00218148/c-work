#pragma once
#include "Animal.h"

using namespace std;

class Cat : public Animal
{
	string name;
public:
	Cat(string name);
	void print();
	void speak();
	~Cat();
};


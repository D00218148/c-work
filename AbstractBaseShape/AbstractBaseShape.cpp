// AbstractBaseShape.cpp				March 2020
// Demonstrates defining Pure Virtual Functions (=0) in a Base class
// Defining one (or more) pure virtual functions makes a class an "Abstract class".
// Derived classes (subclasses) must implement the pure virtual function
// inherited from the Base class.
//
// When the draw() method is called using a pointer of
// Base class type, it is not known which concrete implementation
// of the draw() method will be called until runtime.
// If the object pointed at is a Circle object, then the draw() defined in Circle is called.
// If the object pointed at is a Rectangle object, then the draw() function 
// defined in Rectangle is called.  This is called
// "Run Time Binding" or "Late Binding", because the version of draw() called is not known
// until the type of the object is presented.
/*
 "An abstract class contains at least one pure virtual function. 
 You declare a pure virtual function by using a pure specifier (= 0) 
 in the declaration of a virtual member function in the class declaration."
*/

#include <iostream>
#include <vector>
#include "Animal.h"
#include "Dog.h"
#include "Cat.h"
using namespace std;

// Shape is our Base class in this sample.
// It is an abstract class, because it contains a 
// pure virtual function (=0) (with no implementation).
// We cannot make an instance of Shape because it contains a pure virtual function.
// Every class that derives from Shape must implement the draw() method.

// Note: classes are usually n their own files, but we keep them all in one file
// here for convenience of reading.

class Shape {
public:
	virtual void draw() = 0;  // the "=0" makes this a "pure virtual function"
	virtual ~Shape();	
};
Shape::~Shape() { cout << "~Shape() called. Base destructor is called after derived destructor.\n"; }

// Circle is a Derived class
// It inherits from the Base class Shape
class Circle : public Shape {   // Circle inherits from Shape.  public inheritance
private:
	int x, y;
	int radius;
public:
	Circle(int x, int y, int radius) {
		this->x = x;
		this->y = y;
		this->radius = radius;
	}
	virtual void draw();
	~Circle();			// derived destructor

};
void Circle::draw() {	// implementation of draw 
	cout << "drawing a Circle...\ndone drawing." << endl;
}
Circle::~Circle() { cout << "~Circle() destructor called.\n"; }

class Rectangle : public Shape {
private:
	int x, y;
	int width, height;
public:
	Rectangle(int x, int y, int width, int height)
	{
		this->x = x;
		this->y = y;
		this->width = width;
		this->height = height;
	}
	virtual void draw();
	~Rectangle();
};
void Rectangle::draw() {	// implementation of draw()
	cout << "drawing a Rectangle...\ndone drawing." << endl;
}
Rectangle::~Rectangle() { cout << "~Rectangle() destructor called.\n"; }

void fillShapesVector(vector<Shape*> &vec)
{
	vec.push_back( new Circle(1, 2, 5) );
	vec.push_back( new Rectangle(8, 6, 20, 25) );
	// etc.. 
}

void testMethod(Animal& animal)
{
	animal.print();
	animal.speak();
	cout << animal << endl;
}

int main() //  polymorphism in Action - polymorphism ONLY works with POINTERS
{
	/*
	Shape* shapePtr;	// pointer of Base type. Can poiint at any derived objects
	
	shapePtr = new Circle(2, 3, 40); // object of derived type
	shapePtr->draw();	// late binding - draw() function determined at runtime
	delete shapePtr;

	shapePtr = new Rectangle(3, 4, 10, 15);
	shapePtr->draw();
	delete shapePtr;

	// Vector of pointers to Shape objects
	vector<Shape*> shapes;

	fillShapesVector(shapes);  

	for (Shape* shapePtr : shapes)
	{
		shapePtr->draw();		// polymorphic behaviour
	}

	// finally, we have to free (delete) the dynamically allocated Shape objects 
	// pointed at by the pointers in the vector, and clear the vector.
	for (Shape* rShapePtr : shapes)
	{
		delete rShapePtr;	// free the memory;
	}
	shapes.clear(); // clear the contents of the vector as the objects it points to have been freed.



	// Shape s;		// won't be allowed by compiler, can't instantiate an abstract class
	// shapePtr = new Shape();  // won't be allowed 
	*/
	Animal a("Charles");
	testMethod(a);

	Dog d("Spot", "labrador");
	testMethod(d);

	Cat c("Henry");
	testMethod(c);
	return 0;
}
#pragma once
#include "Shape2.h"
class Rectangle2 :
	public Shape2
{
	int w, h;
public:
	Rectangle2(string name = "Rectangle", int w = 3, int h = 3);
	int getWidth();
	int getHeight();
	void setWidth(int w);
	void setHeight(int h);
	virtual double area();
	virtual double perimiter();
	virtual void draw();
	~Rectangle2();
};


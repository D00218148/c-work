#pragma once
class Calculateable //abstract class - has one or more pure virtual methods
{
public:
	virtual double area() = 0; //pure virtual method
	virtual double perimiter() = 0;
};


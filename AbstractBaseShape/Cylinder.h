#pragma once
#include "circle.h"
class Cylinder :
	public circle
{
	double height;
public:
	Cylinder(double height, double radius);
	virtual void getArea();
	~Cylinder();
};


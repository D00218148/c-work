#include "Hopper.h"

Hopper::Hopper(int id, pair<int, int> position, int direction, int size, int hopLength) : Bug(id, position, direction, size)
{
	this->hopLength = hopLength;
}

void Hopper::move()
{
	if (isWayBlocked())
	{
		if (this->direction == 1)
		{
			position.second -= this->hopLength;
			position.second = checkPositionValues(position.second);
		}
		else if (this->direction == 2)
		{
			position.first -= this->hopLength;
			position.first = checkPositionValues(position.first);
		}
		else if (this->direction == 3)
		{
			position.second += this->hopLength;
			position.second = checkPositionValues(position.second);
		}
		else
		{
			position.first += this->hopLength;
			position.first = checkPositionValues(position.first);
		}
	}
}

//checks whether the coordinate value is greater than 9
//or lower than 0. If so, sets the value to be the min or max
//of grid.
int Hopper::checkPositionValues(int value)
{
	if (value < 0)
	{
		value = 0;
		return value;
	}
	else if (value > 9)
	{
		value = 9;
		return value;
	}
	else
	{
		return value;
	}
}

Hopper::~Hopper()
{

}
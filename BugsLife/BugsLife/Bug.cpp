#include "Bug.h"
#include <iostream>

Bug::Bug(int id, pair<int, int> position, int direction, int size)
{
	this->id = id;
	this->position = position;
	this->direction = direction;
	this->size = size;
	this->alive = true;
}

bool Bug::isWayBlocked()
{
	if (this->direction == 1 && position.second == 0)
	{
		return false;
	}
	else if (this->direction == 2 && position.first == 0)
	{
		return false;
	}
	else if (this->direction == 3 && position.second == 9)
	{
		return false;
	}
	else if (this->direction == 4 && position.first == 9)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void Bug::move()
{
	cout << "Bug doesnt know how to move" << endl;
}

Bug::~Bug()
{

}

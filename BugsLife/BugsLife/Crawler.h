#pragma once
#include "Bug.h"

class Crawler : public Bug
{
public:
	Crawler(int id, pair<int, int> position, int direction, int size);
	void move();
	~Crawler();
};
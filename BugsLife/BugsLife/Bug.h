#pragma once
#include <list>

using namespace std;

class Bug
{
protected:
	int id;
	pair<int, int> position;
	int direction;
	int size;
	bool alive;
	list<pair<int, int>> path;
public:
	Bug(int id, pair<int, int> position, int direction, int size);
	virtual void move();
	bool isWayBlocked();
	~Bug();
};
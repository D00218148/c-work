// BugsLife.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include "Crawler.h"
#include "Hopper.h"
#include "Bug.h"
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

void initialise();
void parseLine(const string& line);
void parseCrawler(stringstream& strStream);
void parseHopper(stringstream& strStream);

vector<Bug*> bugVector;

int main()
{
	initialise();
	cout << bugVector.size() << endl;
}

void initialise()
{
	string line;
	ifstream inStream("bugs.txt");

	if (inStream.good())
	{
		while (getline(inStream, line, '\n'))
		{
			parseLine(line);
		}
		inStream.close();
	}
}

void parseLine(const string& line)
{
	try
	{
		stringstream strStream(line);
		string bugType;
		getline(strStream, bugType, ';');
		if (bugType == "C")
		{
			parseCrawler(strStream);
		}
		else
		{
			parseHopper(strStream);
		}
	}
	catch (invalid_argument const& e)
	{
		cout << "Bad input: value provided did not match expected input" << endl;
	}
}

void parseCrawler(stringstream& strStream)
{
	pair<int, int> position;

	try
	{
		string id;
		getline(strStream, id, ';');
		string xCoordinate;
		getline(strStream, xCoordinate, ';');
		string yCoordinate;
		getline(strStream, yCoordinate, ';');
		position.first = stoi(xCoordinate);
		position.second = stoi(yCoordinate);
		string direction;
		getline(strStream, direction, ';');
		string size;
		getline(strStream, size, ';');

		Crawler crawler(stoi(id), position, stoi(direction), stoi(size));
		bugVector.push_back(&crawler);
	}
	catch (const invalid_argument & e)
	{
		cout << "Bad input: value provided did not match expected input" << endl;
	}
}

void parseHopper(stringstream& strStream)
{
	pair<int, int> position;
	
	try
	{
		string id;
		getline(strStream, id, ';');
		string xCoordinate;
		getline(strStream, xCoordinate, ';');
		string yCoordinate;
		getline(strStream, yCoordinate, ';');
		position.first = stoi(xCoordinate);
		position.second = stoi(yCoordinate);
		string direction;
		getline(strStream, direction, ';');
		string size;
		getline(strStream, size, ';');
		string hopLength;
		getline(strStream, hopLength, '\n');
		
		Hopper hopper(stoi(id), position, stoi(direction), stoi(size), stoi(hopLength));
		bugVector.push_back(&hopper);
	}
	catch (const invalid_argument & e)
	{
		cout << "Bad input: value provided did not match expected input" << endl;
	}	
}
#include "Crawler.h"

Crawler::Crawler(int id, pair<int, int> position, int direction, int size) : Bug(id, position, direction, size)
{
	
}
//1234
//NESW
void Crawler::move()
{
	if (isWayBlocked())
	{
		if (this->direction == 1)
		{
			position.second -= 1;
		}
		else if (this->direction == 2)
		{
			position.first -= 1;
		}
		else if (this->direction == 3)
		{
			position.second += 1;
		}
		else
		{
			position.first += 1;
		}
	}
}

Crawler::~Crawler()
{

}
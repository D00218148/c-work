#pragma once
#include "Bug.h"

class Hopper : public Bug
{
protected:
	int hopLength;
public:
	Hopper(int id, pair<int, int> position, int direction, int size, int hopLength);
	void move();
	int checkPositionValues(int value);
	~Hopper();
};
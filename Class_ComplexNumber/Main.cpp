/*  ComplexNumber sample

   Demonstrates Operator Overloading.
   Operator Overloading allows us to define the behaviour of operators (e.g. +.-,*,...)
   for a class type.

   - operator overloading of "+" and "-" operators: (operator+) and (operator-)
   - operator overloading  of global friend operators << and >>
*/

#include <iostream> 
#include "ComplexNumber.h"
using namespace std;

int main()
{
	ComplexNumber c1;
	cout << "Enter a complex number. Format (Number for real, space, number for imaginary): " << endl;

	cin >> c1;			// invokes the overloaded extraction operator ">>" defined in the ComplexNumber class

	cout << "The complex number is ";

	cout << c1;			// invokes the overloaded insertion operator


	ComplexNumber c2(2, 3);
	cout << "Creating a complex number c2 : " <<  c2 << endl;

	ComplexNumber c3 = c1 + c2;	// invoke "operator+"
	cout << "Executing c3 = c1 + c2." << endl;
	cout << "c3: " << c3 << endl;

	cout << "Outputting c2 and c3 using perint() member function" << endl;
	c1.print();
	c3.print();

	cout << "Executing if(c1>c3) which invokes the operator>" << endl;

	if (c2 > c3)			// invoke operator >
		cout << "True" << endl;
	else
		cout << "False" << endl;

	return 0;

	// Exercises (see ComplexNumber.cpp)
}
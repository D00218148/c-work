/* Implementation of Class ComplexNumber:		Mar 2020
   
   Demonstrates Operator Overloading.
   Operator Overloading allows us to define the behaviour of operators (e.g. +.-,*,...) 
   for a class type.

   - operator overloading of "+" and "-" operators: (operator+) and (operator-)
   - operator overloading  of global friend operators << and >>
*/

#include <iostream> 
#include "ComplexNumber.h"

using namespace std;

/* The stream insertion << and stream extraction >> operators are 
   defined (here) outside the class definition,
   and therefore have global scope. They are GLOBAL operators.  
*/

/* Usage: this is invoked by the following pattern 
   " outputStream << complexNumber" 
   */
ostream& operator << (ostream& out, const ComplexNumber& c)
{
	// As this is defined as a 'friend' of ComplexNumber
	// it has direct access to the private member data of the ComplexNumber.

	out << c.real;
	out << "+i" << c.imaginary << endl;
	return out;
}

/* Usage: this is invoked by the following pattern
   " inputStream >> complexNumber"
   */

istream& operator >> (istream& in, ComplexNumber& c)
{
	cout << "Enter Real Part ";
	in >> c.real;
	cout << "Enter Imaginary Part ";
	in >> c.imaginary;
	return in;
}

/* Implementation of the operator+, 
   enabling the addition of two ComplexNumber objects.
   Usage: complexNumberResult = complexNumber1 + complexNumber2
   
   */
ComplexNumber ComplexNumber::operator + (ComplexNumber const& obj) {
	ComplexNumber temp;
	temp.real = real + obj.real;
	temp.imaginary = imaginary + obj.imaginary;
	return temp; // return an object (by value)
}

// 2Do Implement a 'minus' operator: "operator-"
ComplexNumber ComplexNumber::operator - (ComplexNumber const& obj) {
	ComplexNumber temp;
	temp.real = real - obj.real;
	temp.imaginary = imaginary - obj.imaginary;
	return temp; // return an object (by value)
}


/* Implementation of the greater than operator: "operator>"
   Assume that we need only compare the real part of a complex number
*/
bool ComplexNumber::operator > (ComplexNumber const& obj) {
	if (real > obj.real)
		return true;
	else
		return false;
}

// 2Do: Implement a "less than" operator (using real part only)
bool ComplexNumber::operator < (ComplexNumber const& obj) {
	if (real < obj.real)
		return true;
	else
		return false;
}


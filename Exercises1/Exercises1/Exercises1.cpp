// Exercises1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	/* EX1
	string fName, lName;

	cout << "Please enter first name: ";
	//getLine(cin, fName);
	//cin >> fName;

	cout << "Please enter last name: ";
	cin >> lName;

	cout << lName << " " << fName;
	*/

	/*EX2
	int age;

	cout << "Enter age: ";
	cin >> age;

	if (age < 0)
	{
		cout << "Invalid";
	}
	else if (age < 18)
	{
		cout << "Child";
	}
	else if (age < 65)
	{
		cout << "Adult";
	}
	else
	{
		cout << "Senior Citizen";
	}
	*/

	int choice;
	cout << "1. Run Q1\n2. Run Q2\n4. Run Q4\n5. Run Q5\n6. Run Q6\n7. Run Q7" << endl;
	cin >> choice;
	cin.ignore(1000, '\n');
	switch (choice)
	{
		case 1:
		{
			string fName, lName;

			cout << "Please enter first name: ";
			//getLine(cin, fName);
			cin >> fName;

			cout << "Please enter last name: ";
			cin >> lName;

			cout << lName << " " << fName;
			break;
		}
		case 2:
			int age;

			cout << "Enter age: ";
			cin >> age;

			if (age < 0)
			{
				cout << "Invalid";
			}
			else if (age < 18)
			{
				cout << "Child";
			}
			else if (age < 65)
			{
				cout << "Adult";
			}
			else
			{
				cout << "Senior Citizen";
			}
			break;
		case 4:
			for (int i = 1; i <= 10; i++)
			{
				cout << left << setw(3) << i << " * " << i << " = " << (i * i) << endl;
			}
			break;
		case 5:
		{
			int w = 5, h = 10;
			for (int i = 0; i < w; i++)
			{
				for (int j = 0; j < w; j++)
				{
					cout << "*";
				}
				cout << endl;
			}
		}
			break;
		case 6:
		{
			int w = 5, h = 10;
			for (int i = 0; i < w; i++)
			{
				for (int j = 0; j < h; j++)
				{
					cout << "*";
				}
				cout << endl;
			}
		}
			break;
		case 7:
		{
			int w = 15, h = 1, t = 15;
			for (int i = 0; i < w; i++)
			{
				cout << setw(t);
				for (int j = 0; j < h; j++)
				{
					if (i == 0)
					{
						cout << "^";
					}
					else
					{
						if (j == 0)
						{
							cout << "/";
						}
						else if (j == (h - 1))
						{
							cout << "\\";
						}
						else if (i == (w - 1))
						{
							cout << "_";
						}
						else
						{
							cout << " ";
						}
					}
				}
				t -= 1;
				h += 2;
				cout << endl;
			}
		}
			break;
		case 8:
		{
			unsigned int x = UINT32_MAX;
			char c = CHAR_MAX;
			double d = DBL_MAX;
			cout << x << ", " << (int)(c) << ", " << d << endl;
		}
			break;
		case 10:
		{
			string msg = "Holla World";
			string::size_type length = msg.size() + 4;
			string top(length, '*');
			cout << top << endl;
			cout << "| " << msg << " |" << endl;
			cout << top << endl;
		}
			break;
		case 11:
		{
			string msgs[] = { "Holla World", "Game time", "YOLO" };
			string::size_type length = msgs[0].size() + 4;
			for (string s : msgs)
			{
				if (s.size > length)
				{
					length = s.size();
				}
			}

			string top(length + 4, '*');
			cout << top << endl;

			for (string s : msgs)
			{
				string padd(length - s.size(), ' ');
				cout << "| " << s << padd << " |" << endl;
			}
		}
			break;
		default:
			cout << "Invalid input";
	}
	

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu


#pragma once
#include <iostream>

using namespace std;

class Student
{
	int studentID;
	string title;
	string name;
	int* scores;
	string course;
public:
	Student(int studentID, string title, string name, int* scores, string course);
	Student(const Student &other);
	Student();
	int getStudentID();
	string getTitle();
	string getName();
	int* getScores();
	string getCourse();
	int getGrade(int position);
	void setStudentID(int studentID);
	void setTitle(string title);
	void setName(string name);
	void setScores(int newScore, int position);
	void setCourse(string course);
	string toString();
	Student& operator=(const Student& otherStudent);
	bool operator==(const Student& otherStudent);
	bool operator!=(const Student& otherStudent);
	friend ostream& operator<<(ostream& out, Student& student);
	friend istream& operator>>(istream& in, Student& student);
	/*
	-add validation to constructor
	*/
	~Student();
};


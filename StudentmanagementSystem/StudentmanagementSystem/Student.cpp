#include "Student.h"

Student::Student(int studentID, string title, string name, int* scores, string course)
{
	this->studentID = studentID;
	this->title = title;
	this->name = name;
	this->scores = scores;
	this->course = course;
}

Student::Student(const Student& other)
{
	this->studentID = other.studentID;
	this->title = other.title;
	this->name = other.name;
	this->scores = new int[4];
	for (int i = 0; i < 4; i++)
	{
		this->scores[i] = other.scores[i];
	}
	this->course = other.course;
}

Student::Student()
{
	this->studentID = 99999;
	this->title = "none";
	this->name = "default";
	this->scores = new int[4];
	this->course = "default";
}

int Student::getStudentID()
{
	return this->studentID;
}

string Student::getTitle()
{
	return this->title;
}

string Student::getName()
{
	return this->name;
}

int* Student::getScores()
{
	int* temp = new int[4];
	for (int i = 0; i < 4; i++)
	{
		temp[i] = scores[i];
	}
	return temp;
}

string Student::getCourse()
{
	return this->course;
}

void Student::setStudentID(int studentID)
{
	if (studentID >= 1)
	{
		this->studentID = studentID;
	}
	else
	{
		cout << "Invalid ID. Must be greater than or equal to 1" << endl;
	}
}

void Student::setTitle(string title)
{
	if (title.length() >= 2)
	{
		this->title = title;
	}
	else
	{
		cout << "Invalid title. Must be 3+ characters long" << endl;
	}
}

void Student::setName(string name)
{
	if (name.length() >= 4)
	{
		this->name = name;
	}
	else
	{
		cout << "Invalid name. Must be 4+ characters long" << endl;
	}
}

void Student::setScores(int newScore, int position)
{
	if (position >= 0 && position < 4)
	{
		if (newScore >= 0 && newScore <= 100)
		{
			scores[position] = newScore;
		}
		else
		{
			cout << "Invalid score. Must be between 0 and 100" << endl;
		}
	}
	else
	{
		cout << "position must be between 1 and 4" << endl;
	}
}

void Student::setCourse(string course)
{
	if (course.length() >= 4)
	{
		this->course = course;
	}
	else
	{
		cout << "Invalid course. Must be 4+ characters long" << endl;
	}
}

Student& Student::operator=(const Student& otherStudent)
{
	
	if (this == &otherStudent)
	{
		return *this;
	}
	this->studentID = otherStudent.studentID;
	this->title = otherStudent.title;
	this->name = otherStudent.name;
	this->scores = new int[4];
	for (int i = 0; i < 4; i++)
	{
		this->scores[i] = otherStudent.scores[i];
	}
	this->course = otherStudent.course;
	return *this;
}

bool Student::operator==(const Student& otherStudent)
{
	if (this == &otherStudent)
	{
		return true;
	}
	else if (studentID == otherStudent.studentID && title == otherStudent.title
		&& name == otherStudent.name && &scores == &otherStudent.scores && course == otherStudent.course)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Student::operator!=(const Student& otherStudent)
{
	if (this != &otherStudent)
	{
		return true;
	}
	else if (studentID != otherStudent.studentID && title != otherStudent.title
		&& name != otherStudent.name && &scores != &otherStudent.scores && course != otherStudent.course)
	{
		return true;
	}
	else
	{
		return false;
	}
}

ostream& operator<<(ostream& out, Student& student)
{
	out << student.studentID << ": " << student.title << " " << student.name << " Scores:[";
	for (int i = 0; i < 4; i++)
	{
		out << student.scores[i];
		if (i != 3)
		{
			out << ",";
		}
	}
	out << "] " << student.course;
	return out;
}

int Student::getGrade(int position)
{
	return scores[position];
}

istream& operator>>(istream& in, Student& student)
{
	cout << "Enter student ID" << endl;
	in >> student.studentID;
	cout << "Enter student title" << endl;
	in >> student.title;
	cout << "Enter student name" << endl;
	in >> student.name;
	for (int i = 0; i < 4; i++)
	{
		cout << "Enter score " << (i + 1) << endl;
		in >> student.scores[i];
	}
	cout << "Enter students course" << endl;
	in >> student.course;
	return in;
}

Student::~Student()
{
	delete[] scores;
}
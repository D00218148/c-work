#include "Student.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>

void printMenu();
int getChoice();
void addStudent(vector<Student>& students);
void showStudentsInCourse(vector<Student>& students);
void findStudent(vector<Student>& students);
void printStudent(Student& student);
void showFailingStudents(vector<Student>& students);
int getAverageGrade(Student& student);
void printAllFailingStudents(vector<Student>& students);
void storeStudentData(vector<Student>& students);
void readStudentData(vector<Student>& students);
unsigned int gradesToBytes(Student& student);
void parseLine(const string& str, vector<Student>& students);
void showStudents(vector<Student>& students);
void showAllStudents(vector<Student>& students);

int main()
{
    vector<Student> students;
    int choice;
    bool quit = false;

    cout << "Welcome the the Student Management System" << endl;
    readStudentData(students);
    while (!quit)
    {
        printMenu();

        choice = getChoice();
        switch (choice)
        {
        case 0:
            quit = true;
            storeStudentData(students);
            break;
        case 1:
            addStudent(students);
            break;
        case 2:
            showStudents(students);
            break;
        case 3:
            findStudent(students);
            break;
        case 4:
            showFailingStudents(students);
            break;
        }
    }   
}

void showStudents(vector<Student>& students)
{
    int choice = 0;
    cout << "Enter 1 to show all students" << endl;
    cout << "Enter 2 to show students in a course" << endl;
    cout << "Enter 0 to go back" << endl;
    cin >> choice;
    switch (choice)
    {
    case 0:
        return;
    case 1:
        showAllStudents(students);
        break;
    case 2:
        showStudentsInCourse(students);
        break;
    default:
        cout << "The choice entered isn't available" << endl;
        return;
    }
}

void showAllStudents(vector<Student>& students)
{
    for (int i = 0; i < students.size(); i++)
    {
       cout << students.at(i) << endl;
    }
    cout << endl;
}

/*
parseLine takes data from a line in the text file and turns
it into a student object.
*/
void parseLine(const string& str, vector<Student>& students)
{
    stringstream strStream(str);
    string studentIDAsString;
    getline(strStream, studentIDAsString, ';');
    try
    {
        int id = stoi(studentIDAsString);
        string title;
        getline(strStream, title, ';');
        string name;
        getline(strStream, name, ';');
        string scoresAsString;
        getline(strStream, scoresAsString, ';');
        int scores = stoi(scoresAsString.c_str(), nullptr, 16);
        int* scoresArray = new int[4];
        scoresArray[0] = (scores & (255 << 24)) >> 24;
        scoresArray[1] = (scores & (255 << 16)) >> 16;
        scoresArray[2] = (scores & (255 << 8)) >> 8;
        scoresArray[3] = (scores & (255));
        string course;
        getline(strStream, course, '\n');

        Student temp(id, title, name, scoresArray, course);
        students.push_back(temp);
    }
    catch (invalid_argument const& e)
    {
        cout << "Bad input: input doesn't match required type" << endl;
    }
}

void readStudentData(vector<Student>& students)
{
    string line;
    ifstream inStream("Students.txt");
       
    if(inStream.good())
    {
        
        while (getline(inStream, line, '\n'))
        {
            parseLine(line, students);
        }
        inStream.close();
    }
    else
    {
        cout << "Unable to open file or file is empty" << endl;
    }
}

/*
storeStudentData stores all data in vector into
a text file. Currently there is an issue with
scores where the score data isn't stored the way
it should causing a change in score upon reading
data
*/
void storeStudentData(vector<Student>& students)
{
    ofstream outStream("Students.txt");
    if (outStream.good())
    {
        unsigned int grades;
        for (int i = 0; i < students.size(); i++)
        {
            outStream << students.at(i).getStudentID() << ";";
            outStream << students.at(i).getTitle() << ";";
            outStream << students.at(i).getName() << ";";
            grades = gradesToBytes(students.at(i));
            outStream << hex << grades  << dec << ";";
            outStream << students.at(i).getCourse() << "\n";
        }
        outStream.close();
    }
    else
    {
        cout << "Unable to open file" << endl;
    }
}

unsigned int gradesToBytes(Student& student)
{
    unsigned int grades = 0;
    grades = grades | (student.getGrade(0) << 24);
    grades = grades | (student.getGrade(1) << 16);
    grades = grades | (student.getGrade(2) << 8);
    grades = grades | (student.getGrade(3) << 4);
    return grades;
}

/*
showFailingStudents creates a vector of failing students.
If any student has an average grade of less than 40 they
are added to this vector which gets printed out at the end of method
*/
void showFailingStudents(vector<Student>& students)
{
    vector<Student> failingStudents;
    int studentAvg;
    for (int i = 0; i < students.size(); i++)
    {
        studentAvg = getAverageGrade(students.at(i));
        if (studentAvg < 40)
        {
            failingStudents.push_back(students.at(i));
        }
    }
    if (failingStudents.size() == 0)
    {
        cout << "No failing students" << endl;
    }
    else
    {
        printAllFailingStudents(failingStudents);
    }
}

void printAllFailingStudents(vector<Student>& students)
{
    for (int i = 0; i < students.size(); i++)
    {
        cout << students.at(i) << endl;
    }
    cout << endl;
}

int getAverageGrade(Student& student)
{
    int avg = 0;
    int* scores = student.getScores();
    for (int i = 0; i < 4; i++)
    {
        avg += scores[i];
    }
    delete[] scores;
    return avg / 4;
}

/*
findStudent has the user input an id.
This id is compared against all existing students
in vector. If found, a temp student copies the
values of the existing student with same id
and is printed out
*/
void findStudent(vector<Student>& students)
{
    int id;
    bool found = false;
    Student temp;
    cout << "Enter student ID" << endl;
    cin >> id;
    for (int i = 0; i < students.size(); i++)
    {
        if (students.at(i).getStudentID() == id)
        {
            cout << "Student found" << endl;
            found = true;
            temp = students.at(i);
            printStudent(temp);
        }
    }
    if (found == false)
    {
        cout << "Student ID not found" << endl;
    }
}

void printStudent(Student& student)
{
    cout << student << endl;
}

/*
showStudentsinCourse has the user input a course
and students in this course will be printed out
*/
void showStudentsInCourse(vector<Student>& students)
{
    string course;
    cout << "Enter course" << endl;
    cin >> course;
    for (int i = 0; i < students.size(); i++)
    {
        if (students.at(i).getCourse() == course)
        {
            cout << students.at(i) << endl;
        }
    }
    cout << endl;
}

void addStudent(vector<Student>& students)
{
    bool iDExists = false;
    Student student;
    cin >> student;
    for (int i = 0; i < students.size(); i++)
    {
        if (student.getStudentID() == students.at(i).getStudentID())
        {
            iDExists = true;
        }
    }
    if (iDExists == false)
    {
        students.push_back(student);
        cout << "Student added" << endl;
    }
    else
    {
        cout << "Student ID already exists" << endl;
    }
}

/*
getChoice method gets the user choice from the menu
*/
int getChoice()
{
    bool choiceEntered = false;
    int choice;
    while (!choiceEntered)
    {
        cout << "Enter choice:" << endl;
        cin >> choice;
        if (choice == 0 || choice == 1 || choice == 2 || choice == 3 || choice == 4)
        {
            return choice;
        }
        else
        {
            cout << "Invalid choice. Please enter a choice from menu" << endl;
        }
    }
}

void printMenu()
{
    cout << "Enter 1 to add a new student" << endl;
    cout << "Enter 2 to show students" << endl;
    cout << "Enter 3 to find a student" << endl;
    cout << "Enter 4 to show all failing students" << endl;
    cout << "Enter 0 to exit" << endl;
}
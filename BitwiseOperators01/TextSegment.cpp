#include "TextSegment.h"

TextSegment::TextSegment()
{
}
TextSegment::TextSegment(string s)
{
	text = s;
	flags = 0;
}

bool TextSegment::getBold()
{
	return (flags & boldMask) > 0;
}

void TextSegment::setBold(bool val)
{
	if (val)
	{
		flags = flags | boldMask;
	}
	else
	{
		flags = flags & ~boldMask;
	}
}

bool TextSegment::getItalic()
{
	return (flags & italicMask) > 0;
}

void TextSegment::setItalic(bool val)
{
	if (val)
	{
		flags = flags | italicMask;
	}
	else
	{
		flags = flags & ~italicMask;
	}
}

bool TextSegment::getUnderline()
{
	return (flags & underlineMask) > 0;
}

void TextSegment::setUnderline(bool val)
{
	if (val)
	{
		flags = flags | underlineMask;
	}
	else
	{
		flags = flags & ~underlineMask;
	}
}

void TextSegment::print()
{
	cout << text << "(";
	cout << (getBold() ? "Bold," : "");
	cout << (getItalic() ? "Italic," : "");
	cout << (getUnderline() ? "Underline," : "");
	cout << ")" << endl;
}

TextSegment::~TextSegment()
{
}
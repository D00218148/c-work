#pragma once
#include <string>
#include <iostream>

using namespace std;

class TextSegment
{
	int flags;
	string text;
	const int boldMask = 1;
	const int italicMask = 1 << 1;
	const int underlineMask = 1 << 2;

public:
	TextSegment();

	TextSegment(string s);
	bool getBold();
	bool getItalic();
	bool getUnderline();
	void setBold(bool);
	void setItalic(bool);
	void setUnderline(bool);
	void print();

	~TextSegment();
};


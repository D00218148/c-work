// BitwiseOperators01.cpp : 
/*
Demonstrates:
Bitwise operators:
AND, OR, XOR, NOT
Shift left and Right
*/
#include <iostream>
#include "TextSegment.h"
using namespace std;

void print(string message, int x);
unsigned int toARGB(unsigned char& a, unsigned char& r, unsigned char& g, unsigned char& b);
void getChannels(int colour, unsigned char& a, unsigned char& r, unsigned char& g, unsigned char& b);
void checkOdd(int number);
bool validate(int data);
bool isEven(int x);


int main()
{
    std::cout << "Bitwise Operators\n";
    // The following initializations are equivalent.
    // int x = 15;     // decimal
    int x = 0x0f;      // hexadecimal (each hex digit represents 4 bits)
    //int  x = 0b00001111; // binary
    print("Initializing x to 15", x);

    // Left Shift operator <<

    // Moves bits to the left by the number specified
    x = x << 1;  // move all bits one bit to the left, pusing in zeros
    print("After left shift by 1 bit" , x);
    x = x >> 1; // move bits to right by one bit, pushing in zeros
    print("After right shift by one bit", x);

    x = x << 8; // shift left by 8 bits, each byte one place to the left
    print("After shift left by 8 bits.", x);
    
    // Bitwise OR

    x = x | 0x0f; // bitwise OR 
    // sets the rightmost bits of x to "1111", and
    // all other will remain as they were (because the are ORed with 0)
    print("After bitwise OR { x = x | 0x0f }", x);
 
    // Bitwise AND

    int mask = 0x000000ff;  // same as 0xff
    // If we AND x with the above 'mask' value, then the 
    // leftmost 3 bytes will all be set to zero, and the rightmost 
    // byte (8 bits) will be left as it was. (1&0=0, 1&1=1)
    x = x & mask;
    print("After masking out leftmost three bytes", x);

    // Bitwise XOR  (eXclusive OR)

    // XOR - exclusive OR is used to 'toggle' bits on and off.
    // Let's say we want to switch on/off the second most significant bit.
    // We create a toggle mask with this bit set to 1, and we XOR the mask 
    // with the bit pattern we wish to change.

    x = 0x0f;
    mask = 0x02;  // 0x00000010   mask 
    x = x ^ mask; // XOR will toggle bit 2, but leave all other bits as they were.
    // x was 00001111, and after XOR with 00000010, becomes 00001101 (d in hex)
    print("After XOR with 0x02", x);
    x = x ^ mask; // XOR will toggle bit 2 again, resetting it to 1  (on,off,on,off etc)
    print("After a second XOR with 0x02", x);

    //exercise 1
    int x1 = 15;
    cout << "Size of x1: " << sizeof(x1) << " hex: " << hex << x1 << " decimal: " << dec << x1 << endl;

    x1 = x1 << 4;
    cout << "hex: " << hex << x1 << " decimal: " << dec << x1 << endl;
    x1 = x1 >> 2;
    //~ prints out the opposite of a variable
    cout << ~x1 << endl;
    //isolate a set of bits
    x1 = x1 &255;
    cout << "hex: " << hex << x1 << dec << endl;

    //change 11000011 to 11001111
    x1 = x1 |12;
    cout << "hex: " << hex << x1 << dec << endl;

    x1 = x1 & (~3);
    cout << "hex: " << hex << x1 << dec << endl;



    //exercise 2
    unsigned char alpha, red, green, blue;
    alpha = 0xff;
    red = 0x0f;
    green = 0xff;
    blue = 0x00;

    int colour = 0;
    cout << hex << colour << endl;
    colour = (alpha<<24)|(red<<16)|(green<<8)|blue;
    cout << hex << colour << endl;

    //ex 3
    unsigned int colour2 = toARGB(alpha, red, green, blue);
    cout << hex << colour << endl;

    //ex 4
    unsigned char a;
    unsigned char r;
    unsigned char g;
    unsigned char b;
    int colour3 = 0xfaca9955;
    getChannels(colour3, a, r, g, b);

    //ex 5
    /*
    for (int i = 0; i <= 10; i++)
    {
        cout << i << " ";
        checkOdd(i);
        cout << endl;
    }
    */
    

    //ex 6 - parity check
    int case1 = 15; //true
    int case2 = 13; //false
    
    //cout << case1 << " " << (validate(case1) ? "True" : "False") << endl;

    //ex 7
    TextSegment segment("Some Text");
    segment.print();
    segment.setBold(true);
    segment.setItalic(true);
    segment.print();
    segment.setBold(false);
    segment.print();
}

bool validate(int data)
{
    int count = 0;
    while (data > 0)
    {
        count += data & 1;
        data >> 1;
    }
    return isEven(count);
}

bool isEven(int x)
{
    return (x & 1) == 0;
}

void checkOdd(int number)
{
    if (number & 1)
    {
        cout << "Odd" << endl;
    }
    else
    {
        cout << "Even" << endl;
    }
}

void getChannels(int colour, unsigned char& a, unsigned char& r, unsigned char& g, unsigned char& b)
{
    //gets each colour from a full colour set
    a = colour >> 24;
    r = (colour >> 16) & 255;
    g = (colour >> 8) & 255;
    b = colour & 255;
    cout << hex << (int)a << endl;
    cout << hex << (int)r << endl;
    cout << hex << (int)g << endl;
    cout << hex << (int)b << endl;
}

unsigned int toARGB(unsigned char& a, unsigned char& r, unsigned char& g, unsigned char& b)
{
    unsigned int colour = 0;
    return colour = (a << 24) | (r << 16) | (g << 8) | b;
}

void print(string message, int x)
{
    cout << message << endl;
    cout << "Value of int x = " << x << "(decimal), " << hex << x << "(hexadecimal)." << dec << endl;
    cout << "sizeof(x)  = " << sizeof(x) << endl;
}




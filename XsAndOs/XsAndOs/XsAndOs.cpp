// XsAndOs.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>;

using namespace std;
const int NOWINNER = 3;
const int P1WIN = 1;
const int P2WIN = 2;

void displayBoard(char*);
int checkWin(char*);

int main()
{
	string p1, p2;
	cout << "Please enter player 1: " << endl;
	getline(cin, p1);
	cout << "Please enter player 2: " << endl;
	getline(cin, p2);

	char* board = new char[9];
	for (int i = 0; i < 9; i++)
	{
		board[i] = ' ';
	}
	bool gameOver = false;
	string currentPlayer = p1;
	int row, col;
	bool invalid = false;
	do
	{
		
		displayBoard(board);
		if (invalid)
		{
			invalid = false;
			cout << "Invalid move" << endl;
		}

		cout << currentPlayer << " please enter row and column" << endl;
		cin >> row >> col;
		char moveChar = currentPlayer == p1 ? 'X' : 'O';

		if (board[(row * 3) + col] == ' ')
		{
			board[(row * 3) + col] = moveChar;
			currentPlayer = currentPlayer == p1 ? p2 : p1;
		}
		else
		{
			invalid = true;
		}

		if (checkWin(board) == P1WIN || checkWin(board) == P2WIN || checkWin(board) == NOWINNER)
		{
			if (checkWin(board) == P1WIN)
			{
				cout << "Player 1 wins" << endl;
				gameOver = true;
			}
			else if (checkWin(board) == P2WIN)
			{
				cout << "Player 2 wins" << endl;
				gameOver = true;
			}
			else
			{
				cout << "No winners" << endl;
				gameOver = true;
			}
		}
		
	} while (!gameOver);

	delete[] board;
}

void displayBoard(char* board)
{
	system("cls");
	for (int r = 0; r < 3; r++)
	{
		for (int c = 0; c < 3; c++)
		{
			if (c != 0)
			{
				cout << "|";
			}
			cout << board[(r * 3) + c];
		}
		if (r != 2)
		{
			cout << endl << "-----" << endl;
		}

	}
	cout << endl;
}

int checkWin(char* board)
{
	bool gameOver = true;
	for (int i = 0; i < 9; i++)
	{
		if (board[i] == ' ')
		{
			gameOver = false;
		}
	}
	if (gameOver)
	{
		return 3;
	}

	if (board[0] == board[4] && board[4] == board[8] && board[8] != ' ')
	{
		return board[0] == 'X' ? P1WIN : P2WIN;
	}
	else if (board[2] == board[4] && board[4] == board[6] && board[6] != ' ')
	{
		return board[2] == 'X' ? P1WIN : P2WIN;
	}
	
	for (int i = 0; i < 3; i++)
	{
		if (board[(i*3) + 0] == board[(i * 3) + 1] && board[(i * 3) + 1] == board[(i * 3) + 2] && board[(i * 3) + 2] != ' ')
		{
			return board[(i * 3) + 0] == 'X' ? P1WIN : P2WIN;
		}
	}

	for (int i = 0; i < 3; i++)
	{
		if (board[i] == board[i+3] && board[i+3] == board[i+6] && board[i+6] != ' ')
		{
			return board[i] == 'X' ? P1WIN : P2WIN;
		}
	}
	
	return 0;
}

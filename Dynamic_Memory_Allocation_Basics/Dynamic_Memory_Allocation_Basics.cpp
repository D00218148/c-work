// Dynamic_Memory_Allocation_Basics.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;
void demo1();
void demo2();
void demo3();
void increaseArray(int[], int);
void increasePointer(int*, int);


int main()
{
    std::cout << "Dynamic Memory Allocation - new, delete, new[] and delete[]\n";
	demo1();  // 
	demo2();
	demo3();

}

void demo1()  // Dynamic Memory Allocation (DMA) (memory allocated form the 'Heap')
{
	/*
	 When we declare a variable normally - ( e.g.  int x;) - the memory is automatically
	 allocated (obtained) for us.  That is to say, the system obtains a piece 
	 of memory for us, and names it x.
	 When the variable's life (duration)  ends - then the mmeory associated with x is 
	 'freed up' or 	 given back to the system automatically.  So, we programmers don't 
	 have to worry about managing the memory.
	 This memory is allocated in the 'Stack' area of memory.
	 (Note: the duration(life) of a local varible in a function is from its declaration in
	 the function until the function returns/ends. )

	 Sometimes it is useful, for various reasons, for our program to make a request at RUNTIME 
	 to obtain a block of memory of a particular size, and for our program to manage this memory.
	 This is called allocating memory dynamically. (or Dynamic Memory Allocation - DMA).
	
	 Dynamically allocated memory comes from the 'Heap' part of memory. (where, incidentally, 
	 all objects are stored in Java).  We allocate memory using the keyword 'new' - 
	 which basically requests a block of memory of the specified size.
	 When finished, we should 'free up' the memory using the keyword 'delete'.
	 The memory isn't actually delete,  but rather, the memory block is freed-up 
	 (i.e. given back to the system so that it can be used again)
	
	 Dynamically allocated memory must be accessed using pointers 
	 (pointers that contain the address of the memory allocated from the Heap)
	
	 It is very important to understand DMA, however, it should only be used where 
	 absolutely necessary, as it is the source of 'Memory Leaks' and other bugs in programs.  
	 A memory leak is where a program allocates a block of memory, but
	 does not free it up after it is finished with that memory block. 
	 (i.e. the programmer forgets to delete the DMA)
	
	 Modern C++ provides many features that reduce the need to use DMA.

	 finally, an example .....

	allocating memory dynamically using 'new'
*/
	char* p = new char;		// i.e. get memory from the Heap the size of one char, and store its returned address in a pointer p
	cout << "P before assigned value: " << *p << endl;

	*p = 'F';	// assign  the letter 'F' into the memory location pointed at by pointer p

	cout << "p:\t" << p << endl;	//value stored in p (i.e the address of the DMA <may appear to be gibberish when displayed>)
									// (will assume its a null terminated string)
	cout << "*p:\t" << *p << endl;	//value stored at address pointed to (i.e. stored in p) by p (should be 'F')

	// now, let's say that we are finished using the DMA.
	// we must remember to free up the memory by calling 'delete' on the pointer

	delete p; //frees up the memory pointed at by p.  (memory leak if not called)

	p = nullptr; //pointers should be set to nullptr after the memory that they point at has been deleted,
				// orherwise, they become what is known as 'dangling pointers'
				// Dangling Pointers point to memory that has been freed up.  
				// Dangling pointers are another major source of errors, as if they
				// are accidently used, then they point at memory that may have been allocated to 
				// another pointer, and hence we would have two pointers unintentionally pointing 
				// to the same block of memory

	// MEMORY LEAK !!
	// The major problem is that, if we forget to delete the memory, then our program continues to 
	// work happily (for a time), and no compiler errors or runtime errors will be displayed.
	// However, every time this function is called, it is allocated a new piece 
	// of DMA from the Heap, and never releases (frees) it. So, eventually, all the memory from the
	// Heap will be consumed, and our program will crash.  This sequence of allocating DMA without 
	// freeing it up (using delete) is called a 'memory leak', as it seems like the computer's
	// memory is leaking away until there is none left!.  An additional problem is that this
	// type of error is extremely difficult to detect.

	// Modern C++ provides Smart Pointers which reduce the chances of memory leaks.


	/* 2DO in class:
	allocate DMA to store time in an unsigned long integer value
	store the address of the memory in a pointer called pTime
	assign the value 1234567890 to the memory
	print the value
	change the vale to 0987654321
	print the value
	DRAW a memory diagram to represent the state of variables
	...finish up
	.. oops, did we forget something?


	DRAW a memory diagram showing two pointers pointing at the same block of memory.


	*/
	unsigned long int* x = new unsigned long int;
	unsigned long int* pTime = x;

	*pTime = 1234567890;
	cout << "X: " << *x << endl;
	*pTime = 987654321;
	cout << "X after change: " << *x << endl;

	delete x;
	pTime = nullptr;
	x = nullptr;

}


void demo2()  // using the NULL pointer "nullptr"  (Good Practice)
{
	/* Microsoft Docs says:
	  "The nullptr keyword represents a null pointer value. Use a null pointer value to indicate that
	   an object handle, interior pointer, or native pointer type does not point to an object."
	*/

	int* p =new int;
	*p = 25;

	cout << "p:\t" << p << endl;	//value stored in p
	cout << "*p:\t" << *p << endl;	//value stored at address pointed to by p

	// When we have finished using a pointer, and we wish to indicate that we no longer 
	// want the pointer to point at some object, then we assign the value nullptr to the pointer.
	// nullptr can be assigned only to pointer type variables.
	// It is good practice to assign nullptr to pointers that do not yet point at an object,
	// or pointers that we are finished using.

	delete p;		// free up mmeory pointed at by p
	p = nullptr;	// setting pointer to null prevents us from mistakenly using it after delete

	cout << "p:\t" << p << " (nullptr shows as the value 00000000)" << endl;	//value stored in p

	// Attempting to dereference a null pointer will cause an exception and crash your program
	// Remove the comment below and try running it
	//
	//cout << "*p:\t" << *p << endl;	//value stored in address pointed to (i.e. stored in p) by p
}

void display_using_array_notation(int px[], int size)
{
	for (int i = 0; i < size; i++)
		cout << px[i] << endl;
}

void display_using_pointer_notation(int * px, int size) 
{
	for (int i = 0; i < size; i++) {
		cout << *px << endl;
		px++;	// increment the pointer
	}
}

void demo3()
{
	int size;
	cout << "enter size for array: ";
	cin >> size;
	int* px = new int[size];	// dynamically allocate array of 'size' ints

	for (int i = 0; i < size; i++) {
		cout << "Enter value " << i << ": ";
		cin >> px[i];	// assign inputted value to array
	}

	for (int i = 0; i < size; i++)
		cout << px[i] << endl;

	display_using_array_notation(px, size);	// pass address of array (memory block)

	display_using_pointer_notation(px, size);

	increaseArray(px, size);
	cout << "Increase Array notation: " << endl;
	display_using_array_notation(px, size);

	increasePointer(px, size);
	cout << "Increase Pointer notation: " << endl;
	display_using_array_notation(px, size);

	// 2DO 
// Write and call afunction increase(px,size) that will increate the value of each element by 2;
// write an array notation and a pointer notation version
	// run and test
}

void increaseArray(int px[], int size)
{
	for (int i = 0; i < size; i++)
	{
		px[i] += 2;
	}
}

void increasePointer(int* px, int size)
{
	int* pStart = px;
	for (int i = 0; i < size; i++)
	{
		*px += 2;
		px++;
	}
	px = pStart;
}



#include <string>;
#include <iostream>;

using namespace std;
void displayBoard(char*);

int main()
{
	string p1, p2;
	cout << "Please enter player 1: " << endl;
	getline(cin, p1);
	cout << "Please enter player 2: " << endl;
	getline(cin, p2);

	char* board = new char[9];
	for (int i = 0; i < 9; i++)
	{
		board[i] = ' ';
	}
	bool gameOver = false;
	string currentPlayer = p1;
	int row, col;
	do
	{
		displayBoard(board);
		cout << currentPlayer << " please enter row and column" << endl;
		cin << row << col;
	} while (!gameOver);

	delete[] board;
}

void displayBoard(char* board)
{
	system("cls");
	for (int r = 0; r < 3; r++)
	{
		for (int c = 0; c < 3; c++)
		{
			if (c != 0)
			{
				cout << "|";
			}
			cout << board[(r * 3) + c];
		}
		if (r != 2)
		{
			cout << endl << "-----" << endl;
		}
		
	}
}
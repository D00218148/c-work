#include "Character.h"

Character::Character(string name, int life, int strength, int intelligence)
{
	this->name = name;
	this->life = life;
	this->strength = strength;
	this->intelligence = intelligence;
	this->tools = new string[10];
	this->numTools = 0;
}
Character::Character(Character& other)
{
	this->name = other.name;
	this->life = other.life;
	this->strength = other.strength;
	this->intelligence = other.intelligence;
	this->tools = new string[10];
	for (int i = 0; i < 10; i++)
	{
		this->tools[i] = other.tools[i];
	}
	this->numTools = other.numTools;
}
Character Character::operator=(Character& other)
{
	this->name = other.name;
	this->life = other.life;
	this->strength = other.strength;
	this->intelligence = other.intelligence;
	this->tools = new string[10];
	for (int i = 0; i < 10; i++)
	{
		this->tools[i] = other.tools[i];
	}
	this->numTools = other.numTools;
}
Character::~Character()
{
	delete[] tools;
}
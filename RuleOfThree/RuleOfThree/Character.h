#pragma once
#include <string>

using namespace std;

class Character
{
	string name;
	int life;
	int strength;
	int intelligence;
	string* tools;
	int numTools;

public:
	Character(string name = "John", int life = 100, int strength = 10, int intelligence = 10);
	Character(Character& other);
	Character operator=(Character& other);
	void setame(string n);
	void addTool(string s);
	friend ostream& operator<<(ostream& out, Character& other);
	~Character();
};
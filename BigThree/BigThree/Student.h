#pragma once
#include "Structures.h"
class Student
{
	Course *course;
	string name;
public:
	Student(Course* c, string name = "Default")
	{
		this->name = name;
		this->course = c;
	}
	Student(Student& stud);
	Student& operator=(Student& other);
	void setCourse(Course *c) { course = c; }
	Course* getCourse() { return course; }
	void setName(string n) { name = n; }
	string getName() { return name; }

	~Student();
};

Student::Student(Student& stud)
{
	this->course = new Course(*stud.course);
}

Student& Student::operator=(Student& other)
{
	this->course = new Course(*other.course);
	return *this;
}

Student::~Student()
{

}


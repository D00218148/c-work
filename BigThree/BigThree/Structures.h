#pragma once
#include <iostream>

using namespace std;
struct Point
{
	int x;
	int y;
};
struct Course
{
	int ID;
	string title;
};

struct Question
{
	string question;
	string answer;
};
// BigThree.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "Polygon.h"

using namespace std;

void resetPolygon(Polygon p, int x)
{
    for (int i = 0; i < x; i++)
    {
        Point origin = { 0,0 };
        p.setPoint(i, origin);
    }
}

int main()
{
    Polygon p(5);
    p.setPoint(0, Point{ 1,2 });
    p.setPoint(1, Point{ 5,3 });
    p.setPoint(2, Point{ 4,1 });
    p.setPoint(3, Point{ 6,7 });
    p.setPoint(4, Point{ 8,9 });

    cout << p << endl;
    resetPolygon(p, 5);

    Polygon p2;
    p2.setPoint(0, { 0,0 });
    cout << "p2 after creation: " << p2 << endl;
    p2 = p;
    cout << "p2 after assignment: " << p2 << endl;
    p2.setPoint(1, { 5,9 });
    cout << "p after change: " << p << endl;
    cout << "p2 after change: " << p2 << endl;
    return 0;
}


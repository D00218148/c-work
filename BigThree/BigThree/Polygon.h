#pragma once
#include "Structures.h"
#include <iostream>

using namespace std;

class Polygon
{
	Point *points;
	int numberOfPoints;
public:
	Polygon(int numPoints=1);
	Polygon(Polygon& other);
	Polygon& operator=(Polygon& other);
	void setPoint(int position, Point p);
	Point getPoint(int x);
	friend ostream& operator<<(ostream& out, Polygon& p);
	~Polygon();
};

ostream& operator<<(ostream& out, Polygon& p)
{
	for (int i = 0; i < p.numberOfPoints; i++)
	{
		out << "[" << p.points[i].x << "," << p.points[i].y << "]";
	}
	return out;
}

Polygon::Polygon(int numPoints)
{
	numberOfPoints = numPoints;
	points = new Point[numPoints];
}

Polygon::Polygon(Polygon& other)
{
	this->numberOfPoints = other.numberOfPoints;
	this->points = new Point[this->numberOfPoints];
	for (int i = 0; i < this->numberOfPoints; i++)
	{
		this->points[i] = other.points[i];
	}
}

Polygon& Polygon::operator=(Polygon& other)
{
	this->numberOfPoints = other.numberOfPoints;
	delete[] this->points;
	this->points = new Point[this->numberOfPoints];
	for (int i = 0; i < this->numberOfPoints; i++)
	{
		this->points[i] = other.points[i];
	}
	return *this;
}

void Polygon::setPoint(int position, Point p)
{
	if (position < numberOfPoints)
	{
		points[position] = p;
	}
}

Point Polygon::getPoint(int x)
{
	return points[x];
}

Polygon::~Polygon()
{
	delete[] points;
}
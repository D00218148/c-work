#pragma once
#include "Structures.h"
class Quiz
{
	string title;
	Question * questions;
	int numQuestions;
	int count;
public:
	Quiz(string title, int numQuestions);
	Quiz(Quiz& other);
	Quiz& operator=(Quiz& other);
	void addQuestion(Question q);
	~Quiz();
};

Quiz::Quiz(string title, int numQuestions)
{
	this->title = title;
	questions = new Question[numQuestions];
	this->numQuestions = numQuestions;
	count = 0;
}

Quiz::Quiz(Quiz& other)
{
	this->title = other.title;
	this->numQuestions = other.numQuestions;
	this->questions = new Question[this->numQuestions];
	for (int i = 0; i < this->numQuestions; i++)
	{
		this->questions[i] = other.questions[i];
	}
	this->count = other.count;
}

Quiz& Quiz::operator=(Quiz& other)
{
	this->title = other.title;
	this->numQuestions = other.numQuestions;
	this->questions = new Question[this->numQuestions];
	for (int i = 0; i < this->numQuestions; i++)
	{
		this->questions[i] = other.questions[i];
	}
	this->count = other.count;
	return *this;
}

void Quiz::addQuestion(Question q)
{
	questions[count] = q;
	count++;
}

Quiz::~Quiz()
{
	delete[] this->questions;
}
#pragma once
class Stack
{
	int* nums;
	int count;
	int capacity;
public:
	Stack(int s = 10);
	Stack(Stack& other);
	Stack& operator=(Stack& other);
	void push(int i);
	void pop();
	int peek();
	int size();
	~Stack();
};

Stack::Stack(int s = 10)
{
	nums = new int[s];
	count = 0;
	capacity = s;
}
Stack::Stack(Stack& other)
{
	this->count = other.count;
	this->capacity = other.capacity;
	this->nums = new int[this->capacity];
	for (int i = 0; i < this->capacity; i++)
	{
		this->nums[i] = other.nums[i];
	}
}
Stack& Stack::operator=(Stack& other)
{
	this->count = other.count;
	this->capacity = other.capacity;
	this->nums = new int[this->capacity];
	for (int i = 0; i < this->capacity; i++)
	{
		this->nums[i] = other.nums[i];
	}
	return *this;
}
void Stack::push(int i)
{
	if (count == capacity)
	{
		int * temp = new int[capacity * 2];
		for (int i = 0; i < count; i++)
		{
			temp[i] = nums[i];
		}
		delete[]nums;
		nums = temp;
		capacity = capacity * 2;

	}
	nums[count] = i;
	count++;
}
void Stack::pop()
{
	count--;
}
int Stack::peek()
{
	return nums[count - 1];
}
int Stack::size()
{
	return count;
}

Stack::~Stack()
{
	delete[] this->nums;
}
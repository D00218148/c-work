// Functions.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <ctime>
#include <string>
using namespace std;

double density(double width, double height, double depth, double mass);
int charToDecimal(char);
int increment(int&);
void swap(int&, int&);
int sumRandom(const int& start, const int& end, int& min, int& max);
void divide(int);
int fibonacci(int);
string toBin(int x);

int main()
{
	/*
	double width = 12.4;
	double height = 45.21;
	double depth = 3.33;
	double mass = 43.123;

	double result = density(width, height, depth, mass);
	cout << "Density of object: " << result << endl;

	char letter = 'a';
	cout << "ASCII value: " << charToDecimal(letter) << endl;

	int number = 10;
	cout << "Before increment: " << number << endl;
	cout << "After increment: " << increment(number) << endl;

	int swap1 = 10;
	int swap2 = 20;
	cout << "Before swap: " << endl << swap1 << ", " << swap2 << endl;
	swap(swap1, swap2);
	cout << "After swap: " << endl << swap1 << ", " << swap2 << endl;

	int min, max;
	cout << "Sum of random integers from range: " << sumRandom(10, 20, min, max) <<  endl;
	cout << "Min: " << min << endl;
	cout << "Max: " << max << endl;

	int numToDivide = 10;
	divide(numToDivide);
	*/

	cout << fibonacci(8) << endl;
	cout << toBin(6) << endl;
}

double density(double width, double height, double depth, double mass)
{
	double volume = width * height * depth;
	return mass / volume;
}

int charToDecimal(char letter)
{
	return static_cast<int>(letter);
}

int increment(int& num)
{
	return num++;
}

void swap(int& num1, int& num2)
{
	int temp = num1;
	num1 = num2;
	num2 = temp;
}

int sumRandom(const int& start, const int& end, int& min, int& max)
{
	srand(time(NULL));
	int sum = 0;
	int diff = end - start + 1;
	for (int i = 0; i < 100; i++)
	{
		int num = rand() % diff + start;
		cout << num << endl;
		sum += num;
		if (i == 0)
		{
			min = max = num;
		}
		if (num < min)
		{
			min = num;
		}
		if (num > max)
		{
			max = num;
		}
	}
	return sum;
}

void divide(int num)
{
	if (num <= 1)
	{
		cout << "Number is less than or equal to 1" << endl;
		return;
	}
	else if (num > 1);
	{
		divide(num/2);
		cout << "Current number: " << num << endl;
	}
}

int fibonacci(int n)
{
	if (n == 0 || n == 1)
	{
		return n;
	}
	return fibonacci(n - 1) + fibonacci(n - 2);
	
}

string toBin(int x)
{
	if (x == 0)
	{
		return "";
	}
	return  toBin(x / 2) + to_string(x % 2);
}